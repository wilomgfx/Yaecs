export interface Product {
  Id: number;
  name: string;
  price:number;
  description:string;
  imagePath:string;
}
